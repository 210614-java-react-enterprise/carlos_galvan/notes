### What is SDLC: Software Development Life Cycle
it is a methodology with clearly defined processes with clearly defined processes for creating high quality software (and lowest cost in the shortest time possible.)
- requirement analysis
- planning
- software design (architecural design)
- software development 
- testing
- deployment
- maitnance 

populate SDLC models include Waterfall, Agile and spiral

# Aggregate Funtions
Like piviting a table in excel; typically used with GROUP BY
- Count
- SUM
- MIN MAX
- AVG  

# GROUP BY 
uses aggregated funtions

# insert into 
this statement is used to insert new records in a table 

```sql
insert into table_name (column_name) values (data_info);
insert into table_name values (data_one, data_two);
```

# indexes 
Indexes are used to retrieve data from the DB more quickly than other wise.
Users cannot see the index, used to speed up searches

Because it does slows down when updating it is recommended to index columns that are frequently searched

```sql
 CREATE INDEX index_name on table_name (column1, column2..);
 -- is used tpcreate indexes in a table
 --add the unique keyword to make unique
```

# Data Definition Language --> DDL
consist of sql commands that can be used to define the database 
- CREATE : is used to create the DB or DB objects (table index, fn, views, stpred procedure and triggers)
- DROP : delete objects from DB
- ALTER : alter structure 
- TRUNCATE : remove records from table (included allocated spaced)
- COMMENT : add comments to the data dictionary
- RENAME : used to rename db object

# DML Data manipulation language
sql commands that deals with manipulation of data present in the DB
- INSERT : insert data into a table
- UPDATE : update existing data in a table
- DELETE : delete records from table

# DCL data control language
Deals with rights and permisisions of the DB system
- GRANT : give user's access privileges to the DB
- REVOKE : withdraws users acces privileges 

# TCL transaction control language
Deals with transactions within the database
- COMMIT  : commits a transaction
- ROLLBACK : rollbacks a transaction 
- SAVEPOINT : sets a save point
- SET TRANSACTION : specify characteristics for the transaction

Transactions group a set of task into a singe exectuion unit.
Each transaction begins with a specific task and ends when all the task int he group successfully complete.
If any task fails then the transaction fails
0 sum game like
```sql
--begin transation
BEGIN TRANSACTION transaction_name;

--set transaction (name)
SET TRANSACTION [ READ WRITE | REAL ONLY];

--commit
COMMIT;

--to release savepoint
RELEASE SAVEPOINT savepoint_name
```
https://www.geeksforgeeks.org/sql-transactions/


what are views?
https://www.geeksforgeeks.org/sql-views/
# VIEWS 
views in sql are a VIRTUAL TABLE

```sql
create view view_name AS 
select * 
from table_name
where condition;

--to replace
CREATE OR REPLACE VIEW view_name AS ...
```

# data dictionary
a data dictionary is a SET of database tables used to store information about the database definition

The dictionary contains information about database objects such as tables, indexes, columns, datatypes, and views

the DD is used by sql server to execute queries and is automatically updated whenever objects are added removed or changed

it can be  used to verify statements

# Relationship | Multiplicity
in a relational db a relationship is formed by correlating rows belonging to different tables. a table relationship is established when a child table defines a foreign key column that references the PK column of the parent tab;e

- one to many: (COMMON) assoctiates a row from a parent table to multiple child table

- one to one - requires the child table PK to be associated with FK and PK

- many to many requires a link table containing 2 FK columns that references the 2 different parent tables (movie_actor demo)

# indexes cont.
Indexes are used to retireve data fast
tables and views can be index
Used to inprove performance
quick lookup for FAQ

# SQL Constraints
constraints are used to specify RULES for the data
used to limit the type of data that can go into the table

Constraints are applied to the column

if any errors then it is aborted
can be COLUMN or Table level

- NOT NULL : ensures that a column cannot have a null value
- UNIQUE : ensures that all values are different
- PRIMARY KEY : Not null and unique
- FOREIGN KEY : prevents action that would destroy links between tables
- CHECK : ensures that the values in a column satisfys a specfific condition
- DEFAULT : set a default value
- CREATE INDEX : used to create and retrieve data from db fast

# CHECK
its a constraint used to limit the value range that can be placed in a column
```sql
Age int,
CHECK ( Age >=18)
```

# VIEWS vs Materialized views
A virtual Table based on result set of a SQL statement
a view i a virtual relation that acts an actial relation

not part of logical relational model 

not stored in DB and are generated upon access

## Materialized views 
the stored results 
useful for Frequently accessed data
refresh with REFRESH 
# referential integrity

changes to the DB maintain relationships specified in out DB (FK)

Referantial intgrity stipulates FK must always reference a valid PK

is a DB concept that is used to build and maintain logical relationship between tables to avoid logical corruption of data.
part of RDBMS

The main concept of Referentian integrity is that it does not allow to add any records in a table that contains the FK unless teh reference table containing a corresponding PK

# RDBMS
relational database management system

# SQL SEQUENCE
a sequence is a set of integers 1,2,3 that are generated and supported by  db systems to produce unique values on demand

- User defined schema bound object that generates a sequence of numeric values

generated in ascending or descending order

```sql
create sequence sequence_name
start with 1
increment by 1
minvalue 0
maxvalue 100
cycle

```
# Structured Query Language

# composite key

a composite key is a compination of two or more columns in a table that can be used to uniquely identify each row in the table when the columns are combined uniqueness is guarenteed 

# NULL
null is a field with a null value that is a field with no value
optional

# JOIN

Allow us to create a singe query which spans across multiple tables in our DB
```sql
select [column]
from [left table]
left / right / full join [right table]
on [join predicate]
```

a join clause is used to combine rows from two or more tables based on a related column between them
- (Inner) Join : returns record that have matching values in both tables
- LEFT (OUTER) JOIN : teturns all records from the left table and the match records from the right table
- RIGHT OUTER JOIN: returns all records from the right table and the matched records from the left table
- FULL outer JOIN returns all records when there is a match in either left or tight table

https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/

# data integrity 
data integrity refers to => changes to db follow specs of the db (complies to constraints)

# relational databases
table based architecture with columbs of the table relating to one another (FK)

# Query clauses
- SELECT
- FROM
- WHERE
- GROUP BY
- HAVING
- ORDER BY

# index cont.
an index is a common way to enhace performance (fast retrieval)
DB stores indexes in memory 

# ACID
properties of transaction
- Atomicity : transtation execute fully or not
- Consistency : db is a consistent state before and after a transaction executes
- Isolation : concurrent transaction should not affect the execution of another
- Durable : Durable commited data is permanent and cannot be rolled back

# Transactio Phenomena

- Dirty read: transaction reads uncommitted data
- Non repeatable read:  Transaction A sees inconsistant result sets, records on the wueries have fields which have changed
- phantom read: transtaction A sees an inconsistent result set 

# NORMALIZATION 
- First Normal Form
    - table has a PK
    - columns holds atomic values
    - no repeated columns
- Second Normal Form
    - removed transitive dependencies
    - no partial dependencies
- Third Normal Form
    - removed transitive dependencies

# JDBC 
Java Database connectivity

its a means for accessing our DB with a Java application

we use Maven to obtain the postgres jdbc driver

# connection 
represents a connection or a session with the DB which allows us to execute SQL staements and repriece their results

create using the DriverManager class static getConnection method

# statements
statements are used for executing static SQL staements and returning the results in produces


```java
DriverManager.getConnection(url, username, password);

Connection con = ConnectionUtil.getConnection();

Statement s = con.createStatement();

ResultSet rs = s.executeQuery("SELECT * FROM GARMENT");
```
# prepared statements 
subinterface of statement but is compiled seperately from the parameters

#callable statement

result set

DAO design patter

data access object
MAVEN
