# Callback funtions
a callback funtion is a function passed into another function as an arguement, which is then invoked inside the outer funtion to complete some kind of routine or action

# XMLHttpRequest
the read only status returns the Numerical HTTP status code of the XHR response
- UNSENT 0
- OPENED 0
- LOADING 200
- DONE 200

# XHR ReadyState 
- unsent
- opened
- headers recieved
- loading
- done

# AJAX 
stands for asynchrounous JS and XML
a programming practice of building more complex and dynamic webpages which prepare and send HTTP requests

Browser provides an XMLHttprequest object inorder to perform HTTP request


# Promise
The Promise object represents the eventual completion (or failure) of an asynchronous operation and its resulting value.
A Promise is in one of these states:

pending: initial state, neither fulfilled nor rejected.
fulfilled: meaning that the operation was completed successfully.
rejected: meaning that the operation failed.

we use the .then() funtion to navigate

# Spread 
Spread syntax (...) allows an iterable such as an array expression or string to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected, or an object expression to be expanded in places where zero or more key-value pairs (for object literals) are expected.

# JavaScript

- implementation of ECMAScript, a scripting-language specification
  - ES6 (ES2015)
    - some ES6 features
    - let, const
    - arrow notation
    - for/of loop
    - symbol
    - template literals
  - ECMAScript 2016, 2017, 2018
  - European Computer Manufacturers Association
- a language we can use in conjunction with our HTML files to add functionality and enhance the capabilities of our webpages
- JavaScript was traditionally interpreted by the browser (most recent browsers have JIT compilers for optimization)
  - without JIT compilation, each time a loop executes it would translate the inner code to machine code
  - with JIT compilation, the inner code would be translated to machine code once and then the machine code would be executed for every iteration of the loop
- loosely typed
- single threaded

# Scope

- local scope (lexical scope)
- global scope
- block scope (ES6 +)

# Declaring a variable

- keywords to declare variable:
  - var
    - declares a variable in the current scope (local or global)
  - let
    - declares a variable in the current scope (global, local, or block)
    - cannot be redeclared
  - const
    - declares a variable in the current scope (global, local, or block)
    - cannot be declared without being assigned
    - cannot be redeclared or reassigned

## Data types

- number (2^64, 64 bit floating point)
- string
- boolean
- null
- undefined
- symbol

Everything else is an object

- object is a combination of key value pairs

```javascript
let cat = {
  name: "Ferdinand",
  breed: "calico",
  age: 2,
};
```

# JavaScript in the Browser

- provides functionality to our webpage
- include js in our webpages in internal script or external script
  - `<script> //js code goes here </script>`
  - `<script src="my-external-script.js"></script>` (this is easier to test, maintain, and reuse)

```javascript
let myVar = "Hello World":

```

# DOM Manipulation

- Document Object Model, a tree-like object representation of our html elements
- provides us a way to interface with our html using javascript

window

- document
  - doctype declaration
  - html
    - head
    - body ...

### Accessing Elements on Our Webpage using JS

The document object has methods which allow us to target specific nodes in the DOM. These DOM methods allow for programmatic access to the tree of nodes.

- getElementById
- getElementsByClassName
- getElementsByName
- getElementsByTagName

We can access or manipulate attributes of our nodes.

- [element].setAttribute("attribute","value")
- [element].[attribute]
  - [element].innerText
  - [element].src
- [element].hasAttribute
- [element].classList

Access Related Nodes

- [element].firstChild
- [element].lastChild
- [element].nextSibling
- [element].previousSibling
- [element].children

Add/Remove elements to the DOM

- document.createElement("tagname")
- [element].removeChild([element])

Events

- types of interactions with our HTML page that we can handle programmatically with JS

ex.

- onchange
- onclick
- onmouseover
- onkeydown
- onload

1. assign the appropriate attribute of the html tag to a function
   - `<button onclick="submitForm()">Click to submit</button>`
2. set the appropriate property of the javascript object to a function
   - `<button id="submit-button">Click to submit</button>`
   - document.getElementById("submit-button").onClick = function(){//... submits form}
3. create an event listener on the element specifying a callback function \*
   - `<button id="submit-button">Click to submit</button>`
   - document.getElementById("submit-button").addEventListener("click", callback, bubbling/capturing)

# AJAX - Asynchronous JavaScript and XML

- programming practice of building more complex and dynamic webpages which prepare and send HTTP requests
- browser provides an XMLHttpRequest object in order to perform HTTP requests

XMLHttpRequest - readyState

- 0 unsent : object created, open method has not yet been invoked
- 1 opened
- 2 headers_received
- 3 loading
- 4 done

```javascript
const xhr = new XMLHttpRequest();
xhr.open("GET", "https://pokeapi.co/api/v2/pokemon/1");
xhr.onreadystatechange = function () {
  if (readyState === 4) {
    // process response
  }
};
xhr.send();
```
# HTML - Hypertext Markup Language

- a markup language used to design the structure of a webpage
- document with tags describing the elements on the page

### Structure of an HTML page

```html
<!DOCTYPE html>
<html>
  <head>
    <!-- provides metadata for our webpage -->
    <title>My super cool web page</title>
    <meta charset="utf-8" />
    <!-- additional information can be provided as metadata for SEO -->
    <!-- some external resources -->
  </head>
  <body>
    <!-- provides elements to be rendered -->
    <div>
      <h1>Welcome to my page!</h1>
      <p>here's some content in a paragraph</p>
    </div>
    <div>
      <h3>Check out my cool list:</h3>
      <ul>
        <li>First item</li>
        <li>Second item</li>
        <li>Third item</li>
      </ul>
    </div>
    <img
      src="https://curiodyssey.org/wp-content/uploads/bb-plugin/cache/Mammals-Raccoon-square.jpg"
      alt="cute little raccoon"
    />
  </body>
</html>
```

### Elements

- each element is represented by an html tag (either starting+ending tag or a self closing tag)
- elements allow us to provide attributes in the form of key value pairs
  - global attributes are attributes that can be included on any element
    - class, id, hidden, style, dir
  - local attributes are attributes that are specific to a particular element
    - href for an a tag, src for img tag
- each element is either rendered on a block level or an inline level
  - block elements
    - p, h1-h6, div, ol/ul, form, hr, br, table
  - inline elements
    - img, span, strong or b, em or i
- semantic elements are elements which provide additional context to the html on our web page (strong, em, aside, main, etc.)
- current version of HTML is HTML 5
  - graphic/media tags: canvas, audio, video
  - semantic elements: aside, main, header, footer
  - structural elements: meter, progress

# CSS - Cascading Style Sheets

- css allows us to define styling to change how the html elements on our webpage are rendered
- "cascading" refers to the idea that all of our styles and style sheets will be combined, with qualities "cascading" to elements based on rules and their precedence, and the html hierarchy

### Including Styling in our HTML

1. Inline
   - we can give elements a style attribute, with its value being a string representing a css rule
   - `<div style="text-align:center">... some content</div>`
2. Internal
   - we include css rules in `<style>` tag in the head of our html doc
3. External
   - link to an external css using `link` tag in the head of our html doc
   - `<link rel="stylesheet" type="text/css" href="my-styles.css">`

- precedence of a css rule is based on the way it is applied to the page (e.g. inline v external) and the selector used when defining the rule
- inline styling takes precedence over internal/external styles
  - !important with an external/internal rule can take precedence over inline styling
- internal and external scripts are loaded into the browser in the order they are defined, when there are conflicting styling rules, the later rules override previous styles

### CSS Selectors

all elements

```css
* {
  color: red;
}
```

tagname - sets the color of all paragraphs to red

```css
p {
  color: red;
}
```

class name

```css
.blue-par {
  background-color: blue;
}
```

```html
<p class="blue-par">Some text we want to have a blue background</p>
```

id

```css
#submit-button {
  border-radius: 5px;
}
```

```html
<button id="submit-button">Click to Submit!</button>
```

#### Pseudo class selectors

- used to define a special state of an element
- `[element]: active`
- `[element]: checked`
- `[element]: visited`

### Properties and Rules

- background-color: orange;
- color: green;
- font-size: 12;
- border: 6px solid red;
- text-align: center;
- font-family, font-style, font-weight
- padding, margin
- display (inline, block, inline-block)
- list-style-type
- position (static, relative, absolute, fixed, sticky)

### CSS Box Model

- the box model is the idea of thinking each html item is a layered box

<img src="https://www.washington.edu/accesscomputing/webd2/student/unit3/images/boxmodel.gif" alt="box model" />

- content, padding, border, margin

---
what are some html atribute?

how to apply css to html?

inline
internal
external

JS data types
There are 6 
Undefined, Boolean, Number, String, BigInt, Symbol
number (2^64, 64 bit floating point)
- string
- boolean
- null
- undefined
- symbol


undefined : typeof instance === "undefined"
Boolean : typeof instance === "boolean"
Number : typeof instance === "number"
String : typeof instance === "string"
BigInt : typeof instance === "bigint"
Symbol : typeof instance === "symbol"

Docker?
platform for developers to develop, deploy, and run applications with containers

Template literals

Difference Block vs inline
block spans 

A block-level element always starts on a new line.

A block-level element always takes up the full width available (stretches out to the left and right as far as it can).

A block level element has a top and a bottom margin, whereas an inline element does not

HTML 

semantic
- header
- footer
<article>
<aside>
<details>
<figcaption>
<figure>
<footer>
<header>
<main>
<mark>
<nav>
<section>
<summary>
<time>

Sudo class selectors
A CSS pseudo-class is a keyword added to a selector that specifies a special state of the selected element(s). For example, :hover can be used to change a button's color when the user's pointer hovers over it.

Box model

Margin
Border
padding
content

Events?
- onclick
- onload
- onchange

# VM vs Containers
both ways we can have partition a single machine into multiple computing environments

**virtual machines each have their own OS** that are integrated with the host machines host operating system with a hypervisor
containers have the ability to share a an operating system while maintaining their own isolated computing environment

can have their own file systems, environment variables, etc.

the hypervisor is not necessary, the guest OS are not necessary
much more lightweight more easily scalable
more efficient use of machine's resources

# Bubbling and capturing
When an event happens on an element, it first runs the handlers on it, then on its parent, then all the way up on other ancestors.

# Scopes of variable
- global
- local
- and block

Garbage collector

# syntax=docker/dockerfile:1
FROM ubuntu:18.04
COPY . /app
RUN make /app
CMD python /app/app.py

Hoisting is JavaScript's default behavior of moving declarations to the top.

# Polymorphism

# bg color

# What is an image
image: the basis of a container 
an ordered collection of root filesystem changes and the corresponding execution parameters for use within a container runtime

# CSS Flex
The flex CSS shorthand property sets how a flex item will grow or shrink to fit the space available in its flex container.

# Node JS
Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser

# Docker hub
centralized library for container images
images available from software vendors, can add your own repositories


# short circuit operators
&& ||

# Access related nodes

# methods we can use with nodes
join
pop last element
push add
shift like pop but first element

# dockers CLI commands
FROM
ADD
COPY
ENV
EXPOSE
Label
WORKDIR
RUN

build 
run
image
container
stop
pull
push

inner text vs 

Duck typing

closure !!!

Functions have to exist, first class citizens
Anything a variable can be, a function can also be
You can have function be assigned to a variable
You can have a function be passed in to a function as a param
You can have a function be returned from a function
Anywhere you can put a single value, you can put a function as well