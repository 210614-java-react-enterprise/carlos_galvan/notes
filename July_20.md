# Docker
what is docker? 
> "software that runs on your computer, like a server.. download an image and then run it on your computer" 
> - wyatt

Platform for developers to develop, deploy, and run applications with containers

> what is a container?

with a VM it has its own OS

containers do not have an OS

Hypervisor its a middleware

Containers are more lightweight, and super scalar
A docker engine acts as a hypervisor 

vertical expanding current resources
Horizontal is adding more machines

1. The first step of creating a container is creating an image
   - you can create it of pull it from docker hub

Dockerfile is a file which describes the changes we're making on the environment to make our image

Keywords:
- FROM
- ADD
- COPY
- ENV
- EXPOSE
- LABEL
- WORKDIR
- RUN

java jar


install docker on ec2 in dev-ops scripts
https://gitlab.com/210614-java-react-enterprise/training-repositories/devops-scripts/-/blob/main/install2.sh

https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html


Dockerfile (same level as pom.xml) >
FROM java:8
EXPOSE 8082
COPY target/spring-boot-demo-1.0-SNAPSHOT.jar .
CMD java -var spring-boot-demo-1.0-SNAPSHOT.jar

install git on ec2 

git clone https://gitlab.com/210614-java-react-enterprise/training-repositories/spring-boot-demo.git

-e ENV_NAME=envvalue

OWASP???