# QC Review Week 2 - 4
# Week TWO
## JDBC
- stands for java database connectivity
- its purpose it for accessing our DB with a java app
- Postgres provides dependcies via MAVEN and postgres jdbc driver
- This allows us to leverage the functionality of the classes and interface


JDBC is a standard Java API for database indipendent connectivity between java and databases like postgres
Common task for JDBC are
- Making a connection to a database
- Creating SQL statements
- Executing SQL queries in the database
- Viewing and Modifying the resulting records
Fundamentally JDBC is a specification that provides a complete set of interfaces that allows for portable access to an underlying database. Java can be used to write different executables such as:
- Java Apps
- Java Applets
- Java **Servlets**
- Java ServerPages (JSPs)
- Enterprose JavaBeans (EJBs)
The JDBC API uses a driver manager and database-specific drivers to provide transparent connectivity to heterogeneous databases
Common JDBC Components:
- DriverManager
- Driver
- Connection
- Statement
- ResultSet
- SQLException

### DriverManager
this class manages a list of database drivers. MAtches connection requests from the java application with the proper database driver using communitaction sub protocol. The first deiver that recognizes a certain subprotocol under JDBC will be used to establish a database Connection.

### Driver
This interface handles the communitcations with the databsase server. You will rarely interact with the driver directly. Intead you use DriverManager objects which manages objects of this type. It also abstracts the details associated with working with Driver objects.

### Connection
This interface with all methods for contacting adatabase. The connection object represents communication context ie all communications wi DB is through connection object only.

- represents a connection or **session** with the database, which allows us to execute sql statements and retrieve their results
- create using the DriverManager class' static getConnection method, allong with our database credentials.

### Statements
You use objects created form this interface to submit the SQL statements to the database. Some derived interfaces accepts parameters in additon to ecevuting stored procedures

- used for executing static SQL statement and returning the results it produces
- execution methods will execute them in the database
- autocommit by defaul is true
- creating a new statement
```java
Connection con = ConnectionUtil.getConnection();

Statement s = con.createStatement();

ResultSet rs = s.executeQuery("SELECT * FROM GARMENT");
```
### PreparedStatements
- subinterface of statement, but it is compiled separately from the parameters to prevent SQL injection.
```java
con.prepareStatement("SELECT * FROM GARMENT WHERE COLOR = ?")

statement.set[ type ]( [1 based index ],[ value ])

statement.setString(1,colorInput)
```
### CallableStatement
- subinterface of prepared statement
- used in order to execute stored procedures of function
```java
Connection con = ConnectionUtil.getHardCodedConnection();

CallableStatement cs = con.prepareCall("{call PROCEDURE_NAME(?,?,...)}"));

cs.set[type]([index], [value]);

cs.execute();
```
### ResultSet
These objects hold data retrieved from a databse after you execute an SQL query using Statement objects. It acts as an iterator to allow you to move through its data
- table of data representing a database result set, returned from a statement
- maintains a cursor pointing to its current row of data
- initially the cursor is positioned before the first row
- next method moves the cursor tot he next row, returns false when there are no more rows in the ResultSet object.

```java
statement.executeQuery()

rs.next();

rs.get[type]([column index]/[column name])
```
### SQL Execptions
This class handles any errors that occur in the database applications.
### Transactions in JDBC
- by default, JDBC will autocommit executed statements
- to create explicit transationcs with JDBC we can turn off auto-commit 
- from thre we are able to use provided connection methods to create savepoints, rollbacks, and commit

# DAO Design pattern
- Provides a standard interface for perfoming data access operations
- promotes loose coupling and allows for interchangeablility between data access code

The Data Access Object (DAO) pattern is a structural pattern that allows us to _isolate the application/business layer from the persistence layer (usually a relational database, but it could be any other persistence mechanism) using an abstract API._

The functionality of this API is to hide from the application all the complexities involved in performing CRUD operations in the underlying storage mechanism. This permits both layers to evolve separately without knowing anything about each other.
--- 
# JUnit
- unit test our code 
- a unit test focuses on a really small portion or the codebase: using JUnit, a single method
- Test should not care what order they are run in

### Test driven development (TDD)
- write unit test first
- watch them fail
- write code t0 satisfy test

## JUnit Annotations
@Test - unit test
@Disabled - ignore a unit test
@BeforeEach - runs before each unit test
@BeforeAll - runs once before all unit tests in the class
@AfterEach
@AfterAll
## JUnit Assert Class
many useful static methods for determinin the condition for which test passes:
- assertEquals()
- assertTrue()
- assertFalse()
- assertArrayEquals()
- assertAll() : takes in a variable amount of Executable args, allows us to execute muiltiple assert methods in the same test, while still getting feedback from all of our test

## Mocking Objects
Objects become difficult to test when they have many dependencies on other objects. If we are writing a test for the dependent object - which has a method which can call many other methods - it can be very difficult to figure out the cause of a problem when we encounter one. If we are able to isolate a particular method's functionality, it becomes much easier to test. We can employ strategies such as creating mocks and spies to try to do this.

**dependent object** - which has a method which can call many other methods

Example - Mocking an Object
**Read More**

# Mockito
Mockito is a tool built expressly for creating mocks and spies for testing. 
**Mock**  is basibally a dummy object which we can add functionality to by stubbing its methods; A mocks default behaivor is to do nothing.
**Spy** A spy is a real object whose methods we are able to override with custom implementation; their default behaivor is that of the real object itself.
```java
@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

	@InjectMocks
	private EmployeeService employeeService;
	
	@Mock
	private EmployeeDao employeeDao;

	@Test
	public void testSuccessfulCreation() {
		Employee newEmployee = new Employee(5, "Joe", 500, "Intern", 2, new Department(2));
		when(employeeDao.createEmployee(newEmployee)).thenReturn(1);
		assertTrue(employeeService.createEmployee(newEmployee));
	}
	
	@Test
	public void testUnsuccessfulCreation() {
		Employee newEmployee = null;
		when(employeeDao.createEmployee(newEmployee)).thenReturn(0);
		assertFalse(employeeService.createEmployee(newEmployee));
	}

}
```
# XML eXtensible Markup Language
- like HTML, its not a programming language its a markup language
- designed to transport and store data in a way that is both human and machine readable
- l**anguage agnostic** : is a software development paradigm where a particular language is chosen because of its appropriateness for a particular task (taking into consideration all factors, including ecosystem, developer skill-sets, performance, etc.), and not purely because of the skill-set available within a development team.

JSON has been gaining popularity over XML for representing data because :
- faster parsing
- more compatible w javascript
- less verbose

### Well formed vs Valid XML
Well formed follows basic syntatic rules:
- begins with XML declarations
- unique root element 
- starting and ending tag mush match
- elements are case sensitive
- elements must be properly nested
A well formed document is not neccesarily valid

Valid XML: XML which follows a predefined structure:
- document type definition
- XML schema definition

Valid XML will also be well informed
**Whats better well informed of valid XML**
Valid XML is better because it emcompasses a well informed XML document and it also follows the document type definition
and the XML schema definition.

### XML Namespace
- anyone can create their own markup with their own tags
- namespaces allows us to differentiate from people using the same tag name for different purposes
`xmlns:prefix = "namespace"`
- Unique identifier URL : so if you have a registered URRL you can associate it with your name space to prevent nameing conflict
- URL doesn't necessarily have anything to do with the namespace itself, just a way to prevent name clashes
```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs=".../xmlSchema">
<employees>
    <employee>
        <firstName>John</firstName> <lastName>Doe</lastName>
    </employee>
    <employee>
        <firstName>Anna</firstName> <lastName>Smith</lastName>
    </employee>
    <employee>
        <firstName>Peter</firstName> <lastName>Jones</lastName>
    </employee>
</employees> 

```
# Week Three
## Threads 

# Week Four

## Servlets
a servlet is a class that handles HTTP requests
## Browset / Server communication HTTP
- http stands for HyperText Transfer Protocol
- defines how messages are formated and transmitted accross the network

## HTTP Request
- HTTP Method
- Version of http protocol
- request headers (opt)
- URI
- request body (optional)

## HTTP responses
- version of HTTP Protocol
- response headers (optional)
- response body (optional)
- response code:
  - 100 informational
  - 200 success
  - 300 redirect
  - 400 client
  - 500 server

## HTTP Methods
1. GET
   - used for retrieving data
   - append parameters to url
   - technically a body exist in the message, but any body would have no semantic meaning
   - limited in size of request
   - can be bookmarked
2. POST
   - used to create resources
   - cannot be bookmarked
   - information is sent in request body
3. PUT
   - used to update resources
4. DELETE
   - used to remove resources
5. HEAD
   - returns the headers that would be returned with a get request but without any of the actual body content
6. TRACE
   - used for diagnostics, echos back user input to user 
7. OPTION
   - used to describe the communications options for the target resources

## Servlet inheritence tree
Servlet (interface)
- init
- service
- destroy
GenericServlet (Abstract class)
- service need to be overridem, only abstract class
HttpServlet (Abstract class, no Abstract methods)
- implements service methods
- has handler methods for HTTP verbs(methods) but implemented to return a 405

## Handler Method
Create a webapp directory inside the main directory.
Create a WEB-INF directory
Create a web.xml file inside the WEB-INF folder
```xml
<web-app>
    <context-param>
        <param-name>debug</param-name>
        <param-value>true</param-value>
    </context-param>
	<servlet>
        <init-param>
            <param-name>debug</param-name>
            <param-value>true</param-value>
        </init-param>
		<servlet-name>HelloWorldServlet</servlet-name>
		<servlet-class>com.revature.servlets.HelloWorldServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
	</servlet>
	<servlet-mapping>
		<servlet-name>HelloWorldServlet</servlet-name>
		<url-pattern>/hello</url-pattern>
	</servlet-mapping>
</web-app>
```
## Request and Response Object
Request
- Session
  - request.getSession() obtains the current session, if one does not exist, create one
  - request.getSession(false) : Obtains the current session, if one does not exist it creates one
  - Session.setAttribute Session.getAttribute Session.getLastAccessedTime
  - Session.invalidate()
- getMethod
- getHeader
- getHeader(name)
- getRequestURI
- getRequestDispatcher
  - allows us to send our request and response objects to another servlets
  - forward
  - include

Responses
- getWriter (printWriter)
  - allows us to write into the response body
  - print append
- sendRedirect
- addHeader
- getHeader
- getHeaders
- getStatus



URI stands for Uniform Resource Identifyers

what is <T>? Optional<T>
persistence layer?
Coupling? decoupled
Iterator?
What is SQL injection?
What is a store procedure?
heterogeneous databases?
What is an interface?
what is a query?
what is MAVEN?
