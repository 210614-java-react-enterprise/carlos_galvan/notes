# Functional Programming in Java
- java OOP traditionally
- With JAVA 8 implements tools to enable funtional style programming with Lambdas and Streams

## Functional Programming
- Funtional programming is a programming paradigm in which we try to bind everything in pure mathematical functional style. 
- It is a declarative type of programming style
- Main focus is *WHAT* to solve instead of *HOW* to solve

- It uses *expressions* instead of statements
    - AN expression is evaluated to produce a value whereas a statement is executed to assign variables

## Lambdas Calculus

A lambda is an anonymous inline implementation of a functional interface using arrow notations

- *anonymous inline implementations* : defining the implementation of an interface w/o the need for defining an entireily new class in your program
- *Funtional interfaces* : a functional interface is an interface with only one abstract method
- *Arrow Notation* : arrow notation is a way to abbreviate method declarations

Lambdas are often used as parameters of high order functions meaning functions that take and or return other functions

- forEach method
- Iterable interface
- *Consumer interface*: This is a functional interface and can therefore be used as the assignment targer for a labda expression or method reference; represents an opertaion that accpets a single input argument and returns no result. unlike most other funtional interfaces, Consumer is expected to operate via side-effects;
- *Side effects* : is anything a method does besides computing and returning a value; any change of instance or class field values is a side effect. (drawing on screen, writing to a file, or network connection etc)

- *Function<T, R>* : This is a functional interface and can therefore be used as the assignment targer for a lambda expression or method reference; Represenst a function that accepts one argument and produces a result. 
-*Static* means that the variable or method marked as suc is available at the class level. ie you dont need to create an instance of the class to access is.
-*instance* : is used by objects to store their states; variable that are defined w/o *STATIC* and are outside any method declaration are Object specific and are know as *instacne variables*. They are called so because their values are instance specific and are not shared among instance.

- T - the type of the input to the operations
- R - the type of the result of the function

When we create a lambda wea re implementing the _accept_ method inline, w/o the need for local anonymous class definition. because the _accept_ method takes one parameter and has a vaoid return, our lambda must take one parameter and not return anything. 

- Declaring the datatype for the lambdas parameters are optional, as they are infeered from the functional interface

- When there is only one parameter, the parentheses around it are optional.

- Curly brackets for body of the the lambda are also optional when the return type is void; If the return type is not void, and the braces are not included, the body of the lambda is implied to be returned. (x, y) -> x + y, x + y is implicitly returned.

*Method Reference*: we're telling our program to _apply_ a method to a particulur input; useful when we are only calling upon another method's functionality in our lambda
```java
topics.forEach(System.out::println);
```
## Stream API

Lambdas and streams are designed to work together

*Streams* A stream is a sequence of elements supporting sequentioal or parallel _aggregate_ operations.

We generally use streams to process data; Streams themselves do not store data, we can just think of them as a pipeline for out data. 

A **pipeline** can be made up of three parts:

1. **Stream Generator** (only one)
    - a stream generator converts some data into a _collection_. This is commonly a collection or an arraym but can be also a generator function, an I/) channel, etc.
2. **Intermedite Stream Operation** (optional,and more than one can be used)
    - An intermediate stream operation is an operation which transforms a stream into another stream. These opertaions like 
    - filter
    - map
    - peek etc...
3. **Terminal stream operation** (only one)
    - A terminal stream terminates the pipeline, and returns some result or side effect. These are operations like:
    - findFirst
    - count
    - forEach
    - reduce
    - collect

what is a predicate?

### Intermediate Streams

`Stream<T> filter(Predicate<? super T> predicate)`
**Filter**: (intermediate) returns a stream consisting of the elements of this stream that match the given predicate. its described as lazy (executing an intermediate operation does not actually perform any filtering bu instead creates a new stream that when travered constains the element of the initial stream that match the given predicate)

**Map**: (intermediate) returns a stream consisting of the results of applying the given function of the elements of this stream. (Lazy)

Produces a new stream after applying a function to each element of the OG stream. could be of different type


**forEach** is a _terminal_ operation which means that after the operation is performed the stream pipeline is considered consumed and can no longer be used.

**collect** (Terminal) a common way to get stuff out of the stream one we are done processing. collect() performs mutable fold operations(Repacking elements to some data structures and applying some additional logic, concatenating them, etc) on data elements held in the Stream instance

The strategy for this operation is provided via Collector interface

**findFirst** (terminal) returns an Optional for the first entry in the stream; Optional can be empty

**orElse**

**toArray** like collect(), we can use toArray to get an array out of a Stream.

**flatMap** a stream can hold complex data structure like Stream<List<String>>. in thi case flatmap can help us flatten the data structure to simplify further operations

**peek** (intermediate) sometimes we need to perform multiple operations on each element of the stream before any terminal operation is applied

Peek performs the specified operations on each element of the stream and returns the new stream which can be used further

## Streams as explained on Stackify
streams are wrappers around a data source allowing us to operate with the data source and making bulk processing convesient and fast

A stream does not store data and in that sense is not a data structure. I also never modifies the underlying data source

What is a data structure

java.util.stream this functionality supports functional style operations on streams of elements, such as map-reduce transformation on collections

# Threads
[gitlab page](https://gitlab.com/210614-java-react-enterprise/training-repositories/notes-and-content/-/blob/main/week3-java-cont/threads.md)
a thread is a path of execution in your program. 
represents a subprocesss within you application.

The first preexisting thread in your application is that which is running your **main** method. 

additional one can be started in the MAIN thread

**DEAMON THREADS** threads apart from main like the ones responsible for garbage collection.

The JVM will execute until all non-daemon threads have finished executing.

Threads are defined by implementing the run method in the Runnable interface. This can be done in 2 ways:
1. by extending the _Thread class_ and overriding the run method 
2. by implementing the _Runnable_ interface and providing the implemented class to an instance of the Thread class

