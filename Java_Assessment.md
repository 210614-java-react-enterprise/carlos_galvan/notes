# What is a Singleton Design Pattern

# Collection vs Collections
- Where do map, tree, list and sets fall into
- What is the root of Collection API
- What is in the Collection Class
- Does Map implements the Collection?
- Collection vs Map
- [ ] 
# Annotations
- valid locations
- what are they?

# Predicate Interface
- what is it

# Threads 
- how to create
- stack and heap
- Runnable
- threadsafe?
**Virtual State**

# Intermediate Stream Operations

# Functional Interface
- single Abstract method 

- map() : purpose

# Lambda Expression
- define
- syntax (valid)
- method reference

# DAO interface
- decouple?

# Consumer Interface

# Reflection
- What is it
- drawbacks
- benefits
- instantiating object?

# HashMap vs HashTable
- hashcode?

# Coupling

# Temperal
- LocalDate
- LocalDateTime

# Terminal Operations
## forEach()
- terminal operation
- purpose
## reduce()
## collect()
## findAny()

## filter()

# Streams API
- what are aggregate operations
- Intermediate operation
# Synchronized
- what is it
- how to invoke