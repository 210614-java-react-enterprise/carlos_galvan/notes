## HTTP request:
- version
- host address
- http methods 
- request headers (opt)
- request body (opt)

## HTTP Response:
- version
- response code (200, 404, 500, etc)
- request header (opt)
- request body (opt)


# DAO
interface
# Service Layer
business logig
# Controller / Servlets
interacts with our client via HTTP with Java Servlets

### Needed Tech:
- tomcat (webserver)
- smart tomcat plugin
- postman

# Servlets

[Java Docs for Servlets](https://docs.oracle.com/javaee/7/api/javax/servlet/Servlet.html)

a class that can handle HTTP requests

When a service is created we use the init()
we can paramaterize if we want to

servlets are lazy; must be set to start on startup if you want it to 
