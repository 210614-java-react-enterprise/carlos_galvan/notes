# ORM
is a technique (design pattern) of accessing a relational database from OOP languages
# Hibernate
Hibernate ORM enables developers to more easily write applications whose data outlives the application process. As an Object/Relational Mapping (ORM) framework, Hibernate is concerned with data persistence as it applies to relational databases (via JDBC) 

### Persistance
Hibernate ORM is concerned with helping your application to achieve **persistence**. So what is persistence? Persistence simply means that we would like our application’s data to outlive the applications process. In Java terms, we would like the state of (some of) our objects to live beyond the scope of the JVM so that the same state is available later.

### Relational Databases
Specifically, Hibernate ORM is concerned with data persistence as it applies to relational databases (RDBMS)

### The Object-Relational Impedance Mismatch
(aka Paradigm mismatch) is just a fancy way of saying that object models and relational models do not work very well together. RDBMSs represent data in a tabular format (a spreadsheet is a good visualization for those not familiar with RDBMSs), whereas object-oriented languages, such as Java, represent it as an interconnected graph of objects. Loading and storing graphs of objects using a tabular relational database exposes us to 5 mismatch problems…​
- Granularity
- Subtypes (Inheritence)
- Identity
- Associations
- Data Navigations

#### Granularity
Sometimes you will have an object model which has more classes than the number of corresponding tables in the database (we says the object model is more granular than the relational model). Take for example the notion of an Address…​

#### Subtype
Inheritance is a natural paradigm in object-oriented programming languages. However, RDBMSs do not define anything similar on the whole (yes some databases do have subtype support but it is completely non-standardized)…​
#### Identity
A RDBMS defines exactly one notion of 'sameness': the primary key. Java, however, defines both object identity a==b and object equality a.equals(b).
#### Associations
Associations are represented as unidirectional references in Object Oriented languages whereas RDBMSs use the notion of foreign keys. If you need bidirectional relationships in Java, you must define the association twice.

#### Data navigation
The way you access data in Java is fundamentally different than the way you do it in a relational database. In Java, you navigate from one association to an other walking the object network.

This is not an efficient way of retrieving data from a relational database. You typically want to minimize the number of SQL queries and thus load several entities via JOINs and select the targeted entities before you start walking the object network.

Likewise, you cannot determine the multiplicity of a relationship by looking at the object domain model.

# JPA Providers
In addition to its own "native" API, Hibernate is also an implementation of the Java Persistence API (JPA) specification. As such, it can be easily used in any environment supporting JPA including Java SE applications, Java EE application servers, Enterprise OSGi containers, etc.
# Idiomatic persistence
Hibernate enables you to develop persistent classes following natural Object-oriented idioms including inheritance, polymorphism, association, composition, and the Java collections framework. Hibernate requires no interfaces or base classes for persistent classes and enables any class or data structure to be persistent.
# High Performance
Hibernate supports lazy initialization, numerous fetching strategies and optimistic locking with automatic versioning and time stamping. Hibernate requires no special database tables or fields and generates much of the SQL at system initialization time instead of at runtime.

Hibernate consistently offers superior performance over straight JDBC code, both in terms of developer productivity and runtime performance.

# Scalability
Hibernate was designed to work in an application server cluster and deliver a highly scalable architecture. Hibernate scales well in any environment: Use it to drive your in-house Intranet that serves hundreds of users or for mission-critical applications that serve hundreds of thousands

# Reliable
# Extensibility

--- 
## Object
in memory representation of data
## Relational
refers to a relational database, in this case postgres
## Mapper 
something that translate between the 2; historicaly 

# (Potential) Responsibilities of an ORM
- create database
- define tables
- define fields
    - size / length
    - nullable
    - default value
- define foreign keys
- define indices
- define relationships
    - one to many 
    - many to many
    - one to one
- define stored procedures
- class interface
- create table
- insert data
    - convert data types
    - validate data
- retrieve data
    - support join
    - order results  
    - sub queries
    - nested queries
    - lazy loading
    - eager loading
- update data
    - by id
    - using filters
- delete data
    - by id
    - using filters
- Cascading Operations
- Bulk Operations
- Caching
- Transactions
    - commit
    - rollback
    - nested transactions
- Support PostgreSQL
- prevent SQL injection
- remain performant
- support multiple clients
- use multiple databases
- error handling
- logging

# Responsibilities of our ORM
- Create database
- define tables
- define fields 

- define foreign keys

- create tables
- insert data
- retrieve data

--- 
# Hibernate Docs
https://docs.jboss.org/hibernate/orm/current/quickstart/html_single/#preface

```java
protected void setUp() throws Exception {
	// A SessionFactory is set up once for an application!
	final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
			.configure() // configures settings from hibernate.cfg.xml
			.build();
	try {
		sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
	}
	catch (Exception e) {
		// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
		// so destroy it manually.
		StandardServiceRegistryBuilder.destroy( registry );
	}
}
```
The setUp method first builds a org.hibernate.boot.registry.StandardServiceRegistry instance which incorporates configuration information into a working set of Services for use by the SessionFactory. 

The SessionFactory is a thread-safe object that is instantiated once to serve the entire application.

##### Saving Entities
```java
Session session = sessionFactory.openSession();
session.beginTransaction();
session.save( new Event( "Our very first event!", new Date() ) );
session.save( new Event( "A follow up event", new Date() ) );
session.getTransaction().commit();
session.close()
```

testBasicUsage() first creates some new Event objects and hands them over to Hibernate for management, using the save() method. Hibernate now takes responsibility to perform an INSERT on the database for each Event.

```java
session = sessionFactory.openSession();
session.beginTransaction();
List result = session.createQuery( "from Event" ).list();
for ( Event event : (List<Event>) result ) {
    System.out.println( "Event (" + event.getDate() + ") : " + event.getTitle() );
}
session.getTransaction().commit();
session.close();
```
Here we see an example of the Hibernate Query Language (HQL) to load all existing Event objects from the database by generating the appropriate SELECT SQL, sending it to the database and populating Event objects with the result set data.