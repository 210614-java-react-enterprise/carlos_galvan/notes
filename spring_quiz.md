# What is spring?
Spring is a java framework or bean factory that manages beans for us
Spring framework is built on java which relies on dependency injection as an implementation f IOC
# what are beans

# Bean factory
a factory to mangae the majority of our objects in our applications

# what is the "Idea" behind spring
IOC Inversion control
# What is Inversion Control
Taking control away from the user

# Spring uses this to fulfill IOC
Dependency injection
# What is dependency injection
Dependency injection is a fundamental aspect of the Spring framework through which the Spring container "injects" objects into other objects or "dependencies" or spring beans

Basically this allows for loose coupling of components and moves the responsibility of managing components onto the container

# What are the pros for Dependency Injection
- Loose Coupling
- easier to swap implementation
- easier to test
  
# What are the cons for Dependency injection
Harder to trace

# what are the ways to configure beans
Directly in XML or with Spring annotations

# What are the three annotations to configure beans?
- @Component
- @Repository
- @Service

# Type of injections
- setter
- constructor
- field

# More specific types of injecton
when using autowiring:
- byName
- byType

# What are the scopes of Spring Beans
- singleton
- prototype
- request
- session
- global session

# What is a singleton scope?
a spring bean scope that can only every create an object one at a time
# Prototype scope
this scope will create a new object every time

# Request scope
Any request made for a single bean multiple times during a single request will create and use the same one

# Session scope
creates and uses the same bean for a particular user session

# Global scope
used with portlets

# Steps for Spring Bean LifeCycle
1. instantiate bean
2. populate properties
3. set bean name aware
4. set bean factory aware
5. set application context aware
6. bean processor: preinitialization
7. custom init
8. initializing bean interface: after properties set
9. bean post processors: after initialization
10. in use
11. custom destroy
12. destroy

# Spring modules
- spring core
- spring beans
- sprint context
- test
- AOP
- Data
- MVC
- web

# Spring core
provides us with the Application Context

# What is an Application Context
Extends bean factory and provides us with the ability to do dependency injections (and it supports annotations)

# Clients (in terms of DI)
a client is an object that requires a service or dependency

# Service (in terms of DI)
an object injected into a client

# Core spring annotations
- @Component
- @Repository
- @Controller
- @Services
- @Autowired
- @Quilifier
- @Bean
- @Lazy

# Stereotype annotations
- @Component
- @Repository
- @Controller
- @Services

# Spring boo annotations
- @SpringBootApplication
- @EnableAutoConfig
- @Configurations
- @ComponentScans

# What are some Spring MVC Annotations
- @Controller
- @RequestMapping
- @CrossOrigin
- @GetMapping
- @PostMapping
- @PutMapping
- @ExceptionHandler
- @PathVariable

# Spring Cloud annotaions
- @EnableConfigServer
- @EnableEurekaServer
- @EnableDiscoveryClient
- @EnableCircuitBreaker
- @HystrixCommand

# ways to cofigure Spring MVC
Use a web.xml file to configre the url mapping or the dispatcher servlet. Annotations can be used aswell but still have to be configured in the xml file

# Benefits of Spring boot
Spring boot takes an opinionated approach to configuring your spring application. Spring boot will create a tomcat server for you and based on your dependencies will infer what you will nee for you application

# Spring AOP
Aspect Oriented Programming
modularization of cross-cutting concerns

# RestController vs @Controller
A @RestController is the same as controller except that is also implies @ResponseBody

# Join Point 
A point in our application that is being executed that and aspect can operate on
- point in application where code will be injected
- JP is represented by a method execution

# Point Cut
A set of one or more joint points where advice can be executed

# Point Cut Expression
provides the location that should be watched

# Advice 
Tells the aspect what action to take at the point cut

# Types of advice
- before
- after
- after throwing
- after returning
- around

# JSR-303 Annotations
- @NotNull
- @NotBlank
- @Max
- @Min
- @Pattern
- @Past
- @Email

# Beans vs Autowired
@Beans tells spring here is an instance of this class, basically registers the instance while @Autowiring is asking Spring for an instance of the class

# Spring MVC
A spring project that provides the Model View Controller architecture and components that can be used to make loosely coupled web applications and handle http requests

# ORM
Object relational mapping maps java objects to a relational database (SQL table)

# Examples of an ORM
- Spring Data
- JPA
- Hibernate

# Spring Data
unifies and eases access to different kinds of persistence stores, both relational database systems and NoSQL data stores

# The way to configure
- Declare dependencies
- create the properites files
- configure the entity
- configure spring data JPA

# Lazy fetching vs Eager Fetching
Lazy fetching will return a proxy and will not access the data until asked 
Eager fetching will always return the data

# Spring default fetching type
lazy fetching

# JPA
Java Persistence API is an ORM that uses hibernate

# @Entity
used to map the class to a table

# @Table
only used if you class name doesnt match the table name

# @Id
used to identity the primary key of the table

# @Column
Connects a field to a column in the database

# States of an object in hibernate
- Transient 
- persistent 
- detatched

# Hibernate vs JDBC
Hibernate is complex compared to jdbx. if uder a time constraint jdbc would be more beneficieal to use

# layered systems vs code on demand

# intergration testing
# swagger
# transaction annotation