pipeline {
   agent any

   stages {
      stage('checkout'){
          steps {
            script {
                properties([pipelineTriggers([githubPush()])])
            }
            git branch: 'rp58/additional-mapping-for-RPS', url: 'https://gitlab.com/210614-java-react-enterprise/project-2/team-3-project2-public.git'

          }
      }
      stage('clean') {
         steps {
            sh 'mvn clean'
         }
      }
      stage('package') {
         steps {
            sh 'mvn package'
         }
      }
      
      stage('remove previous app from tomcat') {
          steps{
            sh 'rm usr/apache/apache-tomcat-8.5.35/webapps/*.war'
          }
      }
      
      stage('deploy artifact to tomcat'){
          steps{
              sh 'cp target/*.war usr/apache/apache-tomcat-8.5.35/webapps'
        }
        }
    }
}