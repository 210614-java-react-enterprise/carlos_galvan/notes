# Spring AOP
AOP stands for Aspect oriented program

- breaking down program logic into distint cross cutting concerns
- Aspect Oriented Programming: aspect is our unit of modularity
- Aspects handles a cross cutting concerns
  - logging, security, validation, etc
- Annotatte our class with @Aspect and @Component
  - @Aspect is an annotation we get from Aspect J which is a full OAP framework; we just use the AspectJ annotations rather than the xml config

### Joint Point 
- point in application where code will be injected
- JP is represented by a method execution

### Point cut
- predicate specifies which method will be affected
- collection of JP's matching a particular criteria

### Advice
- code to be injected at a join point 
- Types of advice:
  - **@before**: execute before JP
  - **@After**: execute after JP, regardless of JP's success
    - **@AfterReturning** - executes after JP executes successfully
    - **@AfterThrowing** - executes after JP throws an exception
  - **@around**
    - allows for advice to be injected before and after the JP execution
    - most powerful type of advice and should no be used if Before and after could achieve what's need
- Advice is associated with point cuts and run at matching join points
- _JoinPoint_ object can be passed into any aspect method
  - gives access to the target object( the object being advised;this is a Spring AOP proxy object)
  - allows us to get the JP method signature, arguments, etc.
- _ProceedingJointPoint()_ is an object (Subinterface of JointPoint) that can be passed into a method which handles Around advive only
  - allows you to control if.when the join point is actually executed relative to the surrounding advice

### Types of Point Cut Expression
- execution: the most common, method execution
  - `execution([type] [methodSignature(..)])`
  - `execution(* doSomething())`
  - `execution(* set*(..))`
- within: limits to method execution within certain classes
  - `within(com.revature.beans.*)`
- this: limits matching to JP's where the AOP proxy being adviced
- target: limits matching to JP's where the target object is being proxied
- args: limits matching to JP's where arguments are instaces of the given types

---
# Intro to AspectJ
AOP is a programming paradigm that aims to increase modularity by allowing the separation of cross-cutting concerns. It does do by adding additional behavior to existing code w/o modifying code itself. Instead we declare separatelay which code to modify

AspectJ implements both concerns and the weaving of crosscutting concerns using extensions of Java.

### Maven Dependencies
org.aspectj
1. Runtime
2. Weaver

### Aspect Creation
AspectJ provides an implementation of AOP and has three core concepts:
- Join Point
- PointCut
- Advice

- **Aspect**: a modularization of concerns that cuts across multiple objects. Each aspect focuses on a specific cross cutting functionality.
- **Joint Point**: A point during the execution of a script, such as the execution of a method or a property access
- **Advice**: Action taken by an aspect at the particular join point
- **Pointcut**: a regular expression that matches join points. An advice is associated with a pointcut expression and runs at any point that matches the pointcut.

tldr: AOP stands for Aspect Oriented Programming and it is a propramming paradigm designed to increase modularity by allowing the seperation of cross cutting concerns such as logging, security, validation etc etc. AOP uses aspects as unit of modularity. A popular way to implement AOP in java is with the AspectJ library. AspectJ uses 3 core concepts to implement AOP with are Join Point, point cut and advice

--- 
# Spring Core
## Spring Framework
- a framework built on java which relies on dependency injection as an implementation of inversion of control
- Spring = Framework, also Spring = projects built on the framework

### IOC 
- inversion of control:
  - inverts control of the application flow and object creation to a framework to achieve loose coupling
  - abstract design where behaivor is injected into your classes
  - Hollywood principle "dont call us, we call you"
- Service locator:
  - bean controls the instantiation of its dependendcies

### Dependency Injection
- an implementaion of IOC
  - dependencyL some object to be used
  - injection: passing dependency to a dependent object at creation time
  - spring bean is provided its dependencies by the framework
  - decouples configurations from implementation

In spring this is done with the "Spring container" (IOC, or Inversion of control, container) which injects our registered objects, or our "Spring Beans"
- IOC container is represented by an ApplicationContext or BeanFactory
- We can register beans with the IOC so that it may manage the lifecycle of objejcts, providing dependencies where neccessary

## Spring Modules
The spring frameworks is subdivided into different modules
- Spring core
- Spring Beans
- Spring Context

Spring core and beans provide the essential DI features with the BeanFactory interface
Spring context builds on core and beans to add additional functionality; access to ApplicationContext

Additional Modules include:
- Web modules
- AOP modules
- JDBC modules
- ORM modules

Spring Projects: are built on top of a set of modules
- Spring Boot
- Spring Data
- Spring Security 

### BeanFactory vs ApplicationContext
bot represent the IOC container, manage the lifecycle of spring managed objects

BeanFactory
- older
- lazily instatiates our spring beans
- must provide a resource object configured for our beans.xml

Ex
- XmlBeanFactory
`XmlBeanFactory factory = new XmlBeanFactory (new ClassPathResource("Beans.xml"));`

ApplicationContext
- newer 
- eagerly instatiates spring beans
- provides support for annotations
- creates and manages its own resource object

ApplicationContext is a sub-interface which adds more enterprise-specific functionality on top of the basic functionality of the BeanFactory
- Easier intergrations with Spring's AOP features
- Messages resources handling (for use in internationalization)
- Event publication
- Application-layer specific context such as the WebApplicationContext for use in web application

Ex
- ClassPathXmlApplicationContext
- FileSystemXmlApplicationContext
- XmlWebApplicationContext

### Spring Bean
In spring, the objects that form the backbone of your application and that are managed by the Spring IoX container are called beans. The bean is an object that is instatiated, assembled and otherwise managed by a Spring IoX container. Otherwise, a bwan is simpply one of many objects in your application. Beans, and the dependendies among them, are reflected in the configuration metadata uded by a container

A spring bean is an object whose lifecycle is managed by spring's IOC container

## configurations
XML confic
- defined in beans.xml (srx > main > resources > beans.xml)

Annotations config

1. enable Component Scanning in beans.xml
2. Annotate classes with Sterotype annotations
   1. @Component: a general spring managed component
   2. @Repository: DAO layer classes
   3. @Controller: controller layer classes interaction with HTTP responses/request
   4. @Services: a service layer classes

## Bean Scopes 
1. Singleton - single object instance in the IOC container, each consecutive call for this bean from the AC wil return the same object DEFAULT
2. Prototype - many instances of the bean a new instance is created whenever requested from AC

### Bean Wiring
- just as there are varios ways to register beans with our IOC container ther are a few ways we can wire our beans together
- bean wiring is the process of providing configuration to our application to indicate which beans need to be provided as dependencies to one another 
- how we connect or beans via DI

### Setter injection
- uses set____() methods to provide dependencies
- more readable
- doent endure DI because an instance can be created w/o configuring a particular field (no issue if we dont configure), we will still be created ( @required will make sure there is congiguration for that dependency --  prevents NullPointExceptions later down the line)

### Constructor
- uses constructor injection similarly to Angular , when we provide a constructor with the necessary dependencies as a constructor arguments
- does not allow you to create objects until dependencies are ready

### Autowiring
let spring figure out dependencies itself ("Automagically")

### By Type 
automatically determines DI based on the type of  the dependencies and the setter in the class
### By name 
autmagically determines DI based on the name of the dependencies and the setters in the class

### Autowiring using Annotations
enable component scanning
`<context:component-scan base-package="com.ex"></context:component-scan>`
and include @Autowired annotation over the field which needs to be injected
--- 
# Spring Projects
- Spring project are built on spring modules and provide solutions to other issues faced by enterprise applications
- there are a variaty of spring projects 
- the spring projects are also open source 

## Spring Boot
- Spring Boot takes and opioniatted view of building Spring Applications in order to get an application up and running as quickly as possible
- using SPring boot makes it easy to create stand-alone, production-grade spring based applications that you can just run

### Benefits of Spring Boot
- embedded Tomcat server
- provided 'starter' dependecies
- necessary pre-configuration found in application.properties
  - no xml configuration for additional cofig
- Spring boot actuator - provides endpoints for helpful metrics and project information
- Spring boot development tools

### Running a spring boot application
- annotate the class with your main method with `@SpringBootApplication` 
  - @Configuration
  - @EnableAutoConfiguration
  - @ComponentScan
- in main method SpringApplication.run(Driver.class, args)


## Spring Data JPA
- Spring Data JPA is a spring boot application which abstracts our data persistence layer
- provides repository support for the Java Persistance API (JPA)
- eases development of application that need to access JPA data sources

### Importance Interfaces
- CrudRepository
  - PagingAndSortingRepository
    - JpaRepository
--- 
# Spring MVC
- set up spring applications to handle http request

### Workflow
- http request hits our server and goes first to our deployment descriptor
- goes to our front controller servlet
  - or.springframework.web.servlet.DispactherServlet
  - this is a servlet, configure in web.xml
- Dispatcher servlet consults Handler Mapping
  - org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
  - this is a bean, congigure it in our beans.xml
  - also need to enable annotations <mvc:annotation-driven>
- Directs request to the appropriate controller which processes it accordingly
- controller returns a view name or model data
  - consults view resolver if a view name
  - returned to client in response body if not

### Controllers
- @Controller defines a spring bean says class will handle request
- @RequestMapping(method=RequestMethod.GET, value="/")
- @GetMapping("/")
- @PostMapping()
- parameterizing routes allows you to annotatre method parameter
  - @RequestParam("id) int id when id request parameter is provided
  - @PathVariable("id") int id when an id is included in the path (@GetMapping("/"))
  - can still pass in an HttpServletRequest/Response if we want to uor controller method
- return string representing a view name, view resolver will prepend and append configured prefix and suffix
- return objects/text back in the response body when method is annotated with @ResponseBody
- a method parameter annotated with @RequestBody will use jackson map JSON into the annotated Java object
- @ResponseStatus(value=HttpStatus.NOT_FOUND, reasos="some description") can be used to return a particulat htto status to the client 
- @ContollerAdvice annotation can be sued ot globally handle exceptions, this allows us to set response status based on certain exceptions being thrown anywhere in your application


--- 
_what is a cross cutting concern?_
the cross cutting concern is a concern which is applicable throughout the application. this affects the entire application. Eg logging, security, and data transfer are the concerns needed in almost every module of an application, thus they are the cross cutting concerns
what is a predicate?
what is a target object
what is an Aspect
what is a property aspect
OOP vs AOP
OOP and AOP are not mutually exclusive concepts. OOP is fundamental programming paradigm that is the standard way to code especially for java. It is a pp that relies on the concept of classes and onjects. (structure) (reusable code)

AOP deals with aspects, an aspect is a behaivor that cuts through multiple objects.
One of the drawbacks of OOP is that ceratin things will cross cu
OOP inability to address cross-cutting concerns like logging, security, validation, etc
OOP loose couple (components)
AOP tightly coupled and inflexible

what is an interface
